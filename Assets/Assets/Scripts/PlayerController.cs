﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour {
	
	public float speed = 3.0f;
	
	Rigidbody2D body;
	
	void Start()
	{
		body = GetComponent<Rigidbody2D>();
	}
	
	void FixedUpdate() 
	{
		Vector2 moveDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		body.MovePosition((Vector2)transform.position + (moveDirection.normalized * speed * Time.deltaTime));	
	}
}
